package com.bigfans.searchservice.api;

import com.bigfans.framework.model.FTSPageBean;
import com.bigfans.framework.model.PageContext;
import com.bigfans.searchservice.model.Brand;
import com.bigfans.searchservice.service.BrandService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.bigfans.framework.web.BaseController;
import com.bigfans.framework.web.RestResponse;

import java.util.List;

/**
 * 品牌搜索服务
 * @author lichong
 *
 */
@RestController
public class BrandSearchApi extends BaseController {
	
	@Autowired
	private BrandService brandService;

	@GetMapping("/brands")
	public RestResponse search(
			@RequestParam(value = "q") String keyword ,
			@RequestParam(value = "catId" , required = false) String catId ,
			@RequestParam(value = "cp", required=false , defaultValue = "1") Integer cp ) throws Exception{
		PageContext.setCurrentPage(cp.longValue());
		PageContext.setPageSize(10L);
		FTSPageBean<Brand> ftsPageBean = brandService.search(keyword, catId, PageContext.getStart().intValue(), PageContext.getPageSize().intValue());
		List<Brand> brands = ftsPageBean.getData();
		return RestResponse.ok(brands);
	}
}
