package com.bigfans.paymentservice.model.entity;

import com.bigfans.framework.model.AbstractModel;
import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Table;
import java.math.BigDecimal;

/**
 * 
 * @Description:订单实体
 * @author lichong 2014年12月16日下午11:21:36
 *
 */
@Data
@Table(name="Order")
public class OrderEntity extends AbstractModel {

	private static final long serialVersionUID = -2214063268541939413L;
	
	// 用户信息
	@Column(name="user_id")
	protected String userId;
	// 支付信息
	@Column(name="payment_id")
	protected String paymentId;
	// 支付类型ID
	@Column(name="paymethod_code")
	protected String payMethodCode;
	// 支付类型名称
	@Column(name="paymethod_name")
	protected String payMethodName;
	// 运费
	@Column(name="freight")
	protected BigDecimal freight;
	// 商品总价格
	@Column(name="prod_total_price")
	protected BigDecimal prodTotalPrice;
	// 总价格(计算完邮费和各种优惠后的应付款额)
	@Column(name="total_price")
	protected BigDecimal totalPrice;
	// 是否使用积分
	@Column(name="is_points_used")
	protected Boolean isPointUsed;
	// 获得积分
	@Column(name="gained_point")
	protected Float gainedPoint;
	// 使用积分
	@Column(name="used_point")
	protected Float usedPoint;
    // 积分减免金额
    @Column(name="point_deduction_total")
    protected BigDecimal pointDeductionTotal;
	// 优惠劵信息
	@Column(name="is_coupon_used")
	protected Boolean isCouponUsed;
	@Column(name="used_coupon_msg")
	protected String usedCouponMsg;
    // 优惠劵减免金额
    @Column(name="coupon_deduction_total")
    protected BigDecimal couponDeductionTotal;
	// 余额
	@Column(name="is_balance_used")
	protected Boolean isBalanceUsed;
    // 余额减免金额
    @Column(name="balance_deduction_total")
    protected BigDecimal balanceDeductionTotal;
	@Column(name="used_balance")
	protected BigDecimal usedBalance;
	@Column(name = "prod_total_quantity")
	protected Integer prodTotalQuantity;

	public String getModule() {
		return "Order";
	}

}
