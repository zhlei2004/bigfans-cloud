package com.bigfans.paymentservice.dao;

import com.bigfans.framework.dao.BaseDAO;
import com.bigfans.paymentservice.model.Payment;

/**
 * 
 * @Description:
 * @author lichong
 * 2015年4月5日下午6:35:59
 *
 */
public interface PaymentDAO extends BaseDAO<Payment> {

	Payment getByOrder(String uid, String orderId);
	
	Payment getByTradeNo(String tradeNo);
}
