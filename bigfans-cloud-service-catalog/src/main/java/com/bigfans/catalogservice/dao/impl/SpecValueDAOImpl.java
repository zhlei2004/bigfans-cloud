package com.bigfans.catalogservice.dao.impl;

import com.bigfans.catalogservice.dao.SpecValueDAO;
import com.bigfans.catalogservice.model.SpecValue;
import com.bigfans.framework.dao.MybatisDAOImpl;
import com.bigfans.framework.dao.ParameterMap;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * 
 * @Description:
 * @author lichong
 * 2015年5月31日下午9:11:57
 *
 */
@Repository(SpecValueDAOImpl.BEAN_NAME)
public class SpecValueDAOImpl extends MybatisDAOImpl<SpecValue> implements SpecValueDAO {

	public static final String BEAN_NAME = "specValueDAO";

	@Override
	public List<SpecValue> listByIdList(List<String> idList) {
		ParameterMap params = new ParameterMap();
		params.put("idList", idList);
		return getSqlSession().selectList(className + ".list", params);
	}
	
	@Override
	public List<SpecValue> listByOptionId(String optionId) {
		ParameterMap params = new ParameterMap();
		params.put("optionId", optionId);
		return getSqlSession().selectList(className + ".list", params);
	}

	@Override
	public int count(String optionId, String value) {
		ParameterMap params = new ParameterMap();
		params.put("optionId", optionId);
		params.put("value", value);
		return getSqlSession().selectOne(className + ".count", params);
	}

	@Override
	public SpecValue getByValue(String optionId, String value) {
		ParameterMap params = new ParameterMap();
		params.put("optionId", optionId);
		params.put("value", value);
		return getSqlSession().selectOne(className + ".load", params);
	}
}
