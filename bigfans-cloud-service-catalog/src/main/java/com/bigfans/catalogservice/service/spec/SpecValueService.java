package com.bigfans.catalogservice.service.spec;

import com.bigfans.catalogservice.model.SpecValue;
import com.bigfans.framework.dao.BaseService;

import java.util.List;

/**
 * 
 * @Description:SSV -> Spec SpecValue
 * @author lichong
 * 2015年5月30日下午10:56:34
 *
 */
public interface SpecValueService extends BaseService<SpecValue> {
	
	List<SpecValue> listByIdList(List<String> idList) throws Exception;
	
	List<SpecValue> listByOptionId(String optionId) throws Exception;

	boolean exists(String optionId , String value) throws Exception;

	SpecValue getByValue(String optionId , String value) throws Exception;
	
}
