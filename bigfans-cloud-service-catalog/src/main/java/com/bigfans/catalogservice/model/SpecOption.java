package com.bigfans.catalogservice.model;

import com.bigfans.catalogservice.model.entity.SpecOptionEntity;
import com.bigfans.framework.utils.StringHelper;
import lombok.Data;

import java.util.List;

/**
 * @author lichong 2015年5月31日上午9:19:03
 * @Description:商品规格项
 */
@Data
public class SpecOption extends SpecOptionEntity {

    private static final long serialVersionUID = 8617307772698864089L;

    private String catName;
    private String inputTypeLabel;
    private String pid;
    private String pgId;

    /* 收集页面上的值 */
    private List<String> valueList;
    /* 传数据回页面 */
    private List<SpecValue> specValues;

    public String getInputTypeLabel() {
        if (inputType.equals("M")) {
            inputTypeLabel = "手动输入";
        } else if (inputType.equals("L")) {
            inputTypeLabel = "从列表选择";
        }
        return inputTypeLabel;
    }
}
