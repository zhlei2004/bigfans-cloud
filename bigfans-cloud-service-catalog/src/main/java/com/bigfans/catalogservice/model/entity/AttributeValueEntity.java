package com.bigfans.catalogservice.model.entity;

import com.bigfans.framework.model.AbstractModel;
import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Table;

/**
 * 
 * @Title: 
 * @Description: 商品属性值,由系统录入,每个类别下面有很多属性,每个属性可以有很多属性值.创建商品时由用户从选项中选择属性值进行关联
 * @author lichong 
 * @date 2015年10月6日 下午8:28:04 
 * @version V1.0
 */
@Data
@Table(name="AttributeValue")
public class AttributeValueEntity extends AbstractModel {

	private static final long serialVersionUID = -1378965626230927978L;
	@Column(name="option_id")
	protected String optionId;
	@Column(name="value")
	protected String value;
	@Column(name="category_id")
	protected String categoryId;

	@Override
	public String getModule() {
		return "AttributeValue";
	}
	
}
