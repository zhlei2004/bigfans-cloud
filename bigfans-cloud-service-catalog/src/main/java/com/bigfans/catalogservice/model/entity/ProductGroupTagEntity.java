package com.bigfans.catalogservice.model.entity;

import com.bigfans.framework.model.AbstractModel;
import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Table;

@Data
@Table(name = "ProductGroup_Tag")
public class ProductGroupTagEntity extends AbstractModel {

	private static final long serialVersionUID = -6811834593128832769L;

	@Column(name = "pg_id")
	protected String pgId;
	@Column(name = "tag_id")
	protected String tagId;
	@Column(name = "prod_count")
	protected Integer prodCount;

	@Override
	public String getModule() {
		return "ProductGroupTag";
	}

}
