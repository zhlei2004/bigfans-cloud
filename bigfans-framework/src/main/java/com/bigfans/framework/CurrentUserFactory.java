package com.bigfans.framework;

import com.bigfans.framework.utils.JsonUtils;
import com.bigfans.framework.utils.JwtUtils;

import java.util.Map;

/**
 * 
 * @Description:
 * @author lichong
 * 2015年3月28日下午2:11:05
 *
 */
public class CurrentUserFactory {

	public static String createToken(CurrentUser currentUser , String secret){
		return JwtUtils.create(JsonUtils.toJsonString(currentUser), secret);
	}

	public static CurrentUser getCurrentUser(String token , String secret){
		Map<String, Object> claims = JwtUtils.parse(token, secret);
		return new CurrentUser(claims);
	}

}
