package com.bigfans.framework.es.schema;

import org.elasticsearch.common.settings.Settings.Builder;

public class IndexSettingsFilter_NGram_2_10 implements ElasticSchema {

	@Override
	public void build(Builder settingsBuilder) {
		settingsBuilder.put("index.analysis.filter.2_10_ngram_filter.type", "nGram");
		settingsBuilder.put("index.analysis.filter.2_10_ngram_filter.min_gram", 2);
		settingsBuilder.put("index.analysis.filter.2_10_ngram_filter.max_gram", 10);
	}
}
