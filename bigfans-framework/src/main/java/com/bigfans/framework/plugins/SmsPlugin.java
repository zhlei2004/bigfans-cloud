package com.bigfans.framework.plugins;

public interface SmsPlugin extends Plugin {

	String sendVerificationCode(String mobile , String vcode);
	
	String sendProdArrivalNotice(String mobile);
	
	String sendPriceDownNotice(String mobile);
}
