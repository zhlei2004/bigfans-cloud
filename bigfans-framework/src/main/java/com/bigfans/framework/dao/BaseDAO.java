package com.bigfans.framework.dao;

import java.util.List;

import com.bigfans.framework.model.AbstractModel;
import com.bigfans.framework.model.PageBean;

public interface BaseDAO<M extends AbstractModel> {

	int insert(M example);

	int batchInsert(List<M> objList);

	int delete(M obj);

	int update(M obj);

	M load(M e);

	List<M> list(M obj, Long start, Long pagesize);
	
	List<M> list(ParameterMap params , Long start, Long pagesize);
	
	PageBean<M> page(M obj , Long start, Long pagesize);
	
	PageBean<M> page(ParameterMap params , Long start, Long pagesize);

	Long count(M obj);

}
