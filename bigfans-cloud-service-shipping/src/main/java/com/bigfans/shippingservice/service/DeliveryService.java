package com.bigfans.shippingservice.service;

import com.bigfans.framework.dao.BaseService;
import com.bigfans.shippingservice.model.Delivery;


/**
 * 
 * @Description:配送服务接口
 * @author lichong
 * 2014年12月16日上午10:14:08
 *
 */
public interface DeliveryService extends BaseService<Delivery> {
	
}
